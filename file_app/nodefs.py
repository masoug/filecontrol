"""
This is the "filesystem" of sorts that abstracts common operations for filecontrol into a CRUD interface.
This lends to more flexibilty and modularity--that is, the django views are focused on working with the HTTP API while
nodefs deals with the nitty gritty details of permissions, reparenting, checking, etc... of the file/dirnodes.
"""

import hmac, hashlib
from file_app.models import *
from django.core.files.uploadhandler import FileUploadHandler

# TODO: These will eventually live in settings.py
FILE_STORAGE_PATH = (
  "/home/sammy/Web/filecontrol/",
  "/cs/student/masoug/filecontrol",
)
for pth in FILE_STORAGE_PATH:
  if os.path.isdir(pth):
    FILE_STORAGE_PATH = pth
    break

# TODO: make hmac secret as long as the digest
FILE_HMAC_SECRET = b"hmacsecret"

# Custom upload handler
class DirectFileHandler(FileUploadHandler): 
  # TODO: Add try-excepts to handle any errors gracefully!
  def __init__(self, newModels):
    self.hasher = hmac.new(FILE_HMAC_SECRET, digestmod=hashlib.sha256)
    self.currentFilename = str()
    self.currentFile = None
    self.fileModel = None
    self.newModels = newModels

  def receive_data_chunk(self, raw_data, start):
    self.hasher.update(raw_data)
    self.currentFile.write(raw_data)
    self.currentFile.flush()

  def file_complete(self, file_size):
    digest = str(self.hasher.hexdigest())
    print "%s uploaded; %s %s" % (self.currentFilename, digest, str(file_size))
    self.hasher = hmac.new(FILE_HMAC_SECRET, digestmod=hashlib.sha256)
    self.currentFile.close()
    self.fileModel.fileSize = file_size
    self.fileModel.fileSignature = digest
    self.fileModel.parent = None
    self.fileModel.author = None
    self.fileModel.save()
    self.newModels.append(self.fileModel)

  def new_file(self, field_name, file_name, content_type, content_length, charset):
    fileID = str(uuid.uuid4().hex)
    self.currentFilename = os.path.join(FILE_STORAGE_PATH, fileID)
    self.currentFile = open(self.currentFilename, "wb")
    self.fileModel = FileNode()
    self.fileModel.nodeID = fileID
    if not file_name:
      self.fileModel.visibleName = "Untitled File"
    else:
      self.fileModel.visibleName = file_name
    self.fileModel.mimeType = content_type

  def upload_complete(self):
    # TODO: return an uploadedfile object
    pass


# Indicates whether the given nodeID corresponds to a valid Dir/FileNode
# nodeID is the hex string id
# user is a valid User instance for current user. (django.contrib.auth.models.User)
# Returns the Dir/FileNode instance if the nodeID string is valid; otherwise returns None.
def fromID(nodeID, user):
  if DirNode.objects.filter(nodeID=nodeID, author=user).exists():
    return DirNode.objects.get(nodeID=nodeID, author=user)
  elif FileNode.objects.filter(nodeID=nodeID, author=user).exists():
    return FileNode.objects.get(nodeID=nodeID, author=user)
  else:
    return None

# Returns an array of DirNode objects in descending order: e.g. [home, sammy, desktop, ...]
# the dirnode object is a valid DirNode instance
# the user object is a valid User instance representing current user
def pwd(dirnode, user, currList=None):
  if not currList:
    currList = list()
  currList.insert(0, dirnode)
  if activeDir.parent:
    return pwd(activeDir.parent, user, currList)
  else:
    return currList
